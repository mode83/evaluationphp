<?php

function getUsers(){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT u.id, u.email FROM user u WHERE 1') ;

    $response->execute(array());

    return $response;
}

function getUser($userId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('SELECT * FROM user 
    WHERE id = :userId') ;

    $response->execute(array('userId' => $userId));

    return $response;
}


function createUser($bdd, $email, $password){
//    $bdd = dbConnect();

    $encryptedPassword = password_hash($password, PASSWORD_DEFAULT);
    $response = $bdd->prepare ('INSERT INTO `blog`.user(`email`, `password`)
            VALUES (:email, :password)');

    $response->execute(array('email' => $email, 'password' => $encryptedPassword));


    return $response;

}

function updateArticle($articleId, $author_id, $title, $body){
    $bdd = dbConnect();

    $response = $bdd->prepare ('UPDATE `article` SET 
                                `author_id`= :author_id,
                                `title`= :title ,
                                `body`= :body
                                WHERE id = :articleId') ;

    $response->execute(array(   'articleId' => $articleId,
                                'author_id' => $author_id,
                                'body' => $body,
                                'title' => $title
    ));

    return $response;
}



function deleteArticle($articleId){
    $bdd = dbConnect();

    $response = $bdd->prepare ('DELETE FROM `article` WHERE id= :articleId') ;

    $response = $response->execute(array(   'articleId' => $articleId));

    return $response;
}



function dbConnect(){
    try
    {
        return new PDO('mysql:host=localhost;dbname=blog;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }
}

