<?php
$errors = new ArrayObject();
$title="display User";
$userId = getUserIdFromURI();

require('../Model/userRepository.php');

$response = getUser($userId);


if(!userExist($response)){
    $errors->append('Sorry there is no user id ' . $userId);
    displayErrors($errors);
    die;
}

ob_start();
displayErrors($errors);

require('../view/user/displayUserView.php');

$content=ob_get_clean();

require('../view/templateView.php');

$response->closeCursor();


function getUserIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = end($monUrl) ;

    return $authorId;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}

function userExist($response){
    if($response->rowCount() > 0 ){
        return true;
    }
    return false;

}